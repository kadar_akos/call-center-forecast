from data import range_query
import datetime
from dateutil.relativedelta import relativedelta
from predict import main as test
from predict import predictNextMonth
from calendar import monthrange
from sklearn.neighbors import KNeighborsRegressor
from sklearn.grid_search import ParameterGrid
import pickle
import operator
from pymongo import MongoClient
import time


# Function for incrementing the training sets dates
def train_time(time):
    month = time.month - 1 + 1
    year = time.year + month / 12
    month = month % 12 + 1
    day = monthrange(year,month)[1]
    return datetime.datetime(year, month, day, 23)

# Function for incrementing the testing sets dates
def test_time(time):
    month = time.month - 1 + 1
    year = time.year + month / 12
    month = month % 12 + 1
    day = monthrange(year,month)[1]
    start = datetime.datetime(year, month, 1, 0)
    end = datetime.datetime(year, month, day, 23)
    return start, end

# Training for a give interval
def train_interval(model, start, end_train, num_tests):
    print "Evaluating model:", model
    mse_list = [] # List containing mean-squared errors
    mae_list = [] # List containing mean-absolute errors
    counter = 0
    while counter < num_tests:
        counter += 1
        print
        print "Building Dataset"
        print "Begin training at", start
        end_train = train_time(end_train)
        X, Y = range_query(start, end_train, False) # get the dataset for the range
        model.fit(X, Y) # train the model
        with open('model1.dat', 'w') as model_out:
            pickle.dump(model, model_out) # saving the model out

        print "End train at:", end_train
        print "Testing"
        test_start, test_end = test_time(end_train) # Getting the test time interval
        print "Start Testing at:", test_start
        print "End testing at:", test_end
        end = test_end
        mse, mae = test(test_start, end)
        mse_list.append(mse)
        mae_list.append(mae)
    print
    return sum(mse_list)/float(len(mse_list))

# At the beggining of the process this function gets the important dates
# Start training at the first available date
# end the first training after 1 year from the start
# current date
# next month
# TODO smarter way of handling dates:
# TODO Take into consideration the amount of training data
# TODO maybe testing on max 6 months would be enough
def getDates():
    start = COLLECTION.find().sort([("date", 1)]).limit(1) # start the training with the first date in the database
    start = start[0]['date']
    from_start_to_current = relativedelta(CURRENT, start)
    print "FROM_START_TO_CURRENT", from_start_to_current
    end_train = start + relativedelta(months=9) # end the first training at start + 9months
    next_month = CURRENT + relativedelta(months=1)
    num_tests = relativedelta(CURRENT, end_train).years*12+relativedelta(CURRENT, end_train).months-2
    return start, end_train, num_tests, next_month

#TODO This function will provide more models for the main() currently only runs grid search for KNN
def getModel(model_type):
    if model_type == "knn":
        param_grid = iter(ParameterGrid({'n_neighbors':[50, 60, 70, 80, 90],
                                        'algorithm':['ball_tree', 'kd_tree']}))
        algorithm = KNeighborsRegressor()

#TODO Now this only runs the KNN, need to read models and param-grids from the getModel()
def run_experiment(start, end_train, num_tests):
    param_grid = iter(ParameterGrid({'n_neighbors':[50, 60, 70, 80, 90, 100],
                                     'algorithm':['ball_tree', 'kd_tree']})) # Yields a list of permutation of the parameters

    results = {}
    # Go through all the parameter combinations, test themodels, and save the results
    for i in param_grid:
        model = KNeighborsRegressor(algorithm=i['algorithm'], weights="distance", n_neighbors=i['n_neighbors'])
        mse = train_interval(model, start, end_train, num_tests)
        results[mse] = i
        print
    sorted_results = sorted(results.iteritems(), key=operator.itemgetter(0))
    print sorted_results
    # Save out the best model
    best_hyper_params = sorted_results[0][1]
    best_model = KNeighborsRegressor(algorithm=best_hyper_params['algorithm'],
                                     weights="distance",
                                     n_neighbors=best_hyper_params['n_neighbors'])
    return best_model


def main():
    start, end_train, num_tests, next_month = getDates()
    best_model = run_experiment(start, end_train, num_tests)
    #current = datetime.date.today()
    #current = datetime.datetime.combine(current, datetime.datetime.min.time())
    X, Y = range_query(start, CURRENT, False)
    print
    print "Training best model"
    best_model.fit(X,Y)
    best_model_out = open("best_model.dat", "w")
    pickle.dump(best_model, best_model_out)
    best_model_out.close()
    print
    predicted, dates = predictNextMonth(next_month)
    for i in range(0, len(predicted)):
        print dates[i], predicted[i]

if __name__ == "__main__":
    start = time.time() # Start the timer for the whole process
    client = MongoClient('localhost', 27017)
    db = client.forecast
    COLLECTION = db.date_value_holiday_conthour # CONSTANT database
    #current = datetime.date.today()
    #CURRENT = datetime.datetime.combine(current, datetime.datetime.min.time())
    CURRENT = datetime.datetime(2012, 01, 31) # CONSTANT current time
    main()
    end = time.time() # Stop timer for the process
    elapsed = end - start
    print
    print "Elapsed time:", elapsed