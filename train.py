from sklearn.linear_model import Ridge
from sklearn.svm import SVR
from sklearn.neighbors import KNeighborsRegressor, DistanceMetric
from sklearn.grid_search import GridSearchCV
import numpy as np
import pickle
from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, GradientBoostingRegressor, AdaBoostRegressor


def GridSearch(X, Y, model):
    if model == 'knn':
        estimator = KNeighborsRegressor()
        parameters = {'n_neighbors': [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
                                      17,18,19,20,21,22,23,24,25,26,27,28,29,30]}
    if model == 'svm':
        estimator = SVR(kernel='rbf', verbose=True)
        parameters = {'gamma':[1e-3, 1e-4],'C':[1, 10, 100, 1000]}
    if model == 'ridge':
        estimator = Ridge()
        parameters = {'alpha':[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]}
    if model == 'tree':
        estimator = RandomForestRegressor(n_jobs=-1, verbose=True)
        parameters = {'oob_score': [True, False], 'bootstrap': [True, False], 'n_estimators': [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
                                                                                            17,18,19,20,21,22,23,24,25,26,27,28,29,30]}
    print "Performing Grid Search"
    print
    clf = GridSearchCV(estimator, parameters, cv=3, verbose=True)
    clf.fit(X, Y)
    print "Best parameters found on the training set with 3-fold cross-validation :"
    print clf.best_estimator_
    print "Grid scores on the training set"
    for params, mean_score , scores in clf.grid_scores_:
        print round(mean_score,3), round(scores.std(),3), params


def train(X, Y, model, model_name):
    print "Training"
    print
    if model == "ridge":
        clf = Ridge(alpha=1.0)
    if model == "svm":
        clf = SVR(verbose=True, C=10)
    if model == "knn":
        #distance = DistanceMetric.get_metric('euclidean')
        clf = KNeighborsRegressor(n_neighbors=10, algorithm='kd_tree')
    if model == "tree":
        clf = GradientBoostingRegressor(verbose=True,  n_estimators=300, learning_rate=0.01,
                                        random_state=0, max_depth=4, loss='lad', subsample=0.5)
        #clf = AdaBoostRegressor(n_estimators=10000, loss='square', learning_rate=0.5)
    model = clf.fit(X, Y)
    print "Saving Model"
    with open(model_name, 'w') as model_out:
        pickle.dump(clf, model_out)
    print 'Model Saved'
    print
    return model


def test(X, Y, model1, model2, model3, scaler, encoder, X_test):
    shit_days = []
    counter = 0
    X2 = np.c_[X, abs(model1.predict(X))]
    X3 = model2.predict(X2)
    X = np.c_[abs(X), abs(X2), abs(X3)]
    mse = 0
    for i in range(0, len(X)):
        predicted = model3.predict(X[i])
        predicted = abs(scaler.inverse_transform(predicted))
        golden = Y[i]
        #print str(int(predicted))+','+str(golden)
        mse += (predicted-golden)**2
        if (predicted-golden)**2 >= 60:
            counter += 1
            shit_days.append(X_test[i])
    print "MSE", float(mse)/len(X)
    print "Shit:", counter
    shit_out = open('shit_days.dat', 'w')
    shit_out.write(str(shit_days))



###Training a second round by concatenating the predicted vector and the training set
##print "Second Round"
##second_X = clf.predict(processed_X_train)
##second_X = np.c_[abs(processed_X_train), abs(second_X)]
##clf2 = train(second_X, processed_Y_train, "ridge", 'model2.dat')
##
###Training a third round
##print "Third Round"
##third_X = clf2.predict(second_X)
##third_X = np.c_[processed_X_train, abs(second_X), abs(third_X)]
##clf3 = train(third_X, processed_Y_train, "tree", 'model3.dat')
#
#print "Testing"
#test(X_test_code, Y_test, clf, clf2, clf3, scaler, encoder, X_test)

