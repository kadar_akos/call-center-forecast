import pickle
from data import range_query
import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error
import datetime
from math import sqrt
from pymongo import MongoClient
from calendar import monthrange



#Reading in the data and the model
# Teszt adatok beolvasasa
# Tanult model beolvasasa
# Scaler beolvasasa
# Teszteles fuggveny
def main(start, end):
    print "Reading models and data"
    X, Y = range_query(start, end, True)
    model_in = open('model1.dat')
    model = pickle.load(model_in)
    scaler_in = open('scaler.dat')
    scaler = pickle.load(scaler_in)
    print "Running Tests"
    predicted = scaler.inverse_transform(model.predict(X))
    mse = sqrt(mean_squared_error(predicted, Y))
    mae = mean_absolute_error(predicted, Y)
    print "MSE", mse
    print "MAE", mae
    return mse, mae

def predictNextMonth(next_month):
    client = MongoClient('localhost', 27017)
    db = client.forecast
    collection = db.date_value_holiday_conthour
    X = []
    dates = []
    model_in = open(r'best_model.dat')
    model = pickle.load(model_in)
    print "Using model:", model
    print
    print "Forecast"
    print "........"
    print
    scaler_in = open('scaler.dat')
    scaler = pickle.load(scaler_in)
    for i in collection.find({"date": {"$gte": datetime.datetime(next_month.year, next_month.month, 1),
                                       "$lte":datetime.datetime(next_month.year, next_month.month,
                                        monthrange(next_month.year, next_month.month)[1], 23, 30)}}                                      ):
        X.append(i["X"])
        dates.append((i["date"]))
    X = np.array([el for el in X], dtype=float)
    predicted = scaler.inverse_transform(model.predict(X))
    return predicted, dates