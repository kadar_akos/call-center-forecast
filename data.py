from dateutil import parser
import numpy as np
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
import pickle
from pymongo import MongoClient


def write():
    client = MongoClient('localhost', 27017)
    db = client.forecast
    collection = db.date_value_holiday_conthour
    holidays = read_holidays()
    print 'Writing Date Database'
    with open('forecast.txt', 'rb') as csvfile:
        rows = csvfile.readlines()
        data = []
        target = []
        times = []
        for i in rows:
            time = i.split('\t')[0].replace('"','')
            print time

            time = parser.parse(time, fuzzy=True)
            times.append(time)

            #aggregating hours
            morning = 1 if time.hour < 9 else 0
            midday = 1 if time.hour >= 9 and time.hour < 16 else 0
            evening = 1 if time.hour >= 16 and time.hour <= 21 else 0
            late = 1 if time.hour > 21 else 0

            ##aggregating months
            #winter = 1 if time.month == 12 or time.month == 1 or time.month == 2 else 0
            #spring = 1 if time.month == 3 or time.month == 4 or time.month == 5 else 0
            #summer = 1 if time.month == 6 or time.month == 7 or time.month == 8 else 0
            #autumn = 1 if time.month == 9 or time.month == 10 or time.month == 11 else 0

            weekend = 1 if time.weekday() >= 5 else 0
            holiday = 1 if time.date() in holidays else 0
            

            data.append([time.weekday(), time.day, time.hour, time.minute, time.month, holiday,
                        morning, midday, evening, late, weekend])
            

            target.append(int(i.split('\t')[1].replace('\n','')))

        X = np.array([el for el in data], dtype=float)
        Y = np.array([el for el in target], dtype=float)
        
        processed_X, processed_Y = preProcess(X, Y)

        for i in range(0, len(X)):
            a = {}
            a["X"]= processed_X[i].tolist()[0]
            a["Y"]= processed_Y[i]
            a["date"] = times[i]
            a["num_calls"] = float(Y[i])
            collection.insert(a)

def preProcess(X, Y):
    encoder = OneHotEncoder(categorical_features=[0,1,3,4])
    scaler = StandardScaler()
    X = encoder.fit_transform(X).todense()
    Y = scaler.fit_transform(Y)
    with open('scaler.dat', 'w') as scaler_out:
        pickle.dump(scaler, scaler_out)
    with open('encoder.dat', 'w') as encoder_out:
        pickle.dump(encoder, encoder_out)
    return X, Y


def range_query(start, end, test):
    client = MongoClient('localhost', 27017)
    db = client.forecast
    collection = db.date_value_holiday_conthour
    X = []
    Y = []
    dates = []
    for i in collection.find({"date": {"$gte": start, "$lte": end}}):
        X.append(i["X"])
        if test == True:
            Y.append(i["num_calls"])
        if test == False:
            Y.append(i["Y"])

    X = np.array([el for el in X], dtype=float)
    Y = np.array([el for el in Y], dtype=float)
    return X, Y


def read_holidays():
    print "Bulding Holiday list"
    holidays = []
    text = open(r'holidays.txt').readlines()
    for i in text:
        time = parser.parse(i)
        holidays.append(time)
        return holidays

def dropcollection():
    client = MongoClient('localhost', 27017)
    db = client.forecast
    collection = db.date_value_holiday_conthour
    db.drop_collection(collection)