import pickle
from data import range_query
import datetime
import dateutil
import numpy
from sklearn.ensemble import GradientBoostingRegressor
from pybrain.datasets.supervised import SupervisedDataSet as SDS
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from dateutil.relativedelta import relativedelta
from calendar import monthrange


# It's written so that you only change the test_month variable and the further is handeled
def dataloader():
    scaler_in = open('scaler.dat')
    scaler = pickle.load(scaler_in)
    start = datetime.datetime(2011, 01, 01)
    test_month_start = datetime.datetime(2012, 1, 1)
    test_month_end = datetime.datetime(test_month_start.year, test_month_start.month,
                                       monthrange(test_month_start.year, test_month_start.month)[1],
                                       hour=23, minute=30)
    previous = test_month_start - relativedelta(months=1)
    end = datetime.datetime(previous.year, previous.month,
                            monthrange(previous.year, previous.month)[1], hour=23, minute=30)
    X_train, Y_train = range_query(start, end, False)
    X_test, Y_test = range_query(test_month_start, test_month_end, False)
    return X_train, Y_train, X_test, scaler

# This function can train a model from sklearn and save out its predictions
def quick():
    X_train, Y_train, X_test, scaler = dataloader()
    model = GradientBoostingRegressor(verbose=True,  n_estimators=300, learning_rate=0.01,
                                         random_state=0, max_depth=4, loss='lad', subsample=0.5)
    print "training"
    model.fit(X_train, Y_train)
    predicted = scaler.inverse_transform(model.predict(X_test))
    predicted_out = open("predicted2", "wb")
    numpy.save(predicted_out, predicted)

# This function trains a neural network from PyBrain and saves out the predictions
def neural():
    X_train, Y_train, X_test, scaler = dataloader()
    Y_train = Y_train.reshape( -1, 1 )
    dataset = SDS(X_train.shape[1], 1)
    for i in range(0, len(Y_train)):
        dataset.appendLinked(X_train[i], Y_train[i])

    net = buildNetwork(59, 30, 1, bias=True )
    trainer = BackpropTrainer(net, dataset, verbose=True, learningrate=0.05)
    print "nanana"
    trainer.trainUntilConvergence(verbose=True, maxEpochs=40, validationProportion=0.01)
    predicted = []
    for i in X_test:
        predicted.append(scaler.inverse_transform(net.activate(i)))
    predicted = [el for el in predicted]
    predicted_out = open("predicted", "wb")
    numpy.save(predicted_out, predicted)

quick()